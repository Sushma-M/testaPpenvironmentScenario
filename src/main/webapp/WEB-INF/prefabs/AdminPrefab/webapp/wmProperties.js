var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "AdminPrefab",
  "homePage" : "Main",
  "name" : "AdminPrefab",
  "platformType" : "DEFAULT",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "PREFAB",
  "version" : "1.06"
};